from django.urls import path, include
from rest_framework.routers import DefaultRouter
from apps.api.views import UsersViewSet, DiagnosesViewSet

router = DefaultRouter()
router.register(r'diagnoses', DiagnosesViewSet, basename='diagnosis')
router.register(r'users', UsersViewSet, basename='user')

urlpatterns = [
    path('', include(router.urls)),
]