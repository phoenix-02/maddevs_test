from rest_framework_simplejwt.authentication import JWTAuthentication
from django.core.exceptions import PermissionDenied


class CustomJWTAuthentication(JWTAuthentication):
    def authenticate(self, request):
        """
            Customize DEFAULT_AUTHENTICATION_CLASS to check is_doctor inside
            token body
        """
        result = super().authenticate(request)
        if result:
            _, validated_token = result
            if validated_token.payload['is_doctor']:
                return result
            else:
                raise PermissionDenied("You are not a doctor")
