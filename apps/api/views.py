from rest_framework import viewsets

from apps.api.mixins import ViewSetActionPermissionMixin
from apps.api.models import CustomUser, Diagnosis
from apps.api.paginator import PatientsPagination
from apps.api.serializers import (DiagnosisSerializer,
                                  CustomUserListUpdateSerializer,
                                  CustomUserCreateSerializer, )
from rest_framework.permissions import AllowAny, IsAuthenticated


class UsersViewSet(ViewSetActionPermissionMixin, viewsets.ModelViewSet):
    queryset = CustomUser.objects.prefetch_related('diagnoses').filter(
        is_doctor=False
    )

    permission_classes = [IsAuthenticated]
    serializer_class = CustomUserListUpdateSerializer
    pagination_class = PatientsPagination

    # need for multiple serializers and permissions
    action_serializers = {
        'create': CustomUserCreateSerializer,
    }

    permission_action_classes = {
        "create": [AllowAny],
    }

    def get_serializer_class(self):
        if hasattr(self, 'action_serializers'):
            return self.action_serializers.get(self.action,
                                               self.serializer_class)

        return super(UsersViewSet, self).get_serializer_class()


class DiagnosesViewSet(viewsets.ModelViewSet):
    queryset = Diagnosis.objects.all()
    permission_classes = [IsAuthenticated]
    serializer_class = DiagnosisSerializer
