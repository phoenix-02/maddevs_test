<h1>MadDevs test pet-project</h1>
<h2>Hospital patients list</h2>

<h3>get started</h3>
1 `git clone https://gitlab.com/phoenix-02/maddevs_test.git`

2 `cd maddevs_test`

3 create(copy) ".env" file from ".env example"(or copy)

4 copy data.json to project dir for auto load data

5 `docker-compose up`

6 visit `http://127.0.0.1:8000/docs/`

 <h3>usage</h3>     
register as doctor to get permission: 

    users > [post] /users/  

than copy access token:  

    token > [get] /token/  

authorize using right-top button 

paste token with prefix "Bearer "

example:  
`"Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ0b2tlbl90eXBlIjoiYWNjZXNzIiwiZXhwIjoxNjM3MjQzNDExLCJpYXQiOjE2MzcyNDMxMTEsImp0aSI6IjY5NWNlOTJjODE4MjQ3ZmY5YTk3MmYwNGVlM2U0NzY4IiwidXNlcl9pZCI6MSwiaXNfZG9jdG9yIjp0cnVlfQ.y2jPuLikf7FA3r3J2qi_-I7FlSnx1GjR9uzbk4LKo0g"`


view 3 patients: 

    users > [get] /users/  

