from rest_framework import serializers
from django.contrib.auth.password_validation import validate_password

from django.contrib.auth import get_user_model

from apps.api.models import Diagnosis

User = get_user_model()


class DiagnosisSerializer(serializers.ModelSerializer):
    class Meta:
        model = Diagnosis
        fields = ('title',)


class CustomUserCreateSerializer(serializers.ModelSerializer):
    username = serializers.CharField(required=True)
    diagnoses = serializers.PrimaryKeyRelatedField(
        queryset=Diagnosis.objects.all(),
        many=True
    )
    password = serializers.CharField(
        write_only=True,
        required=True,
        validators=[validate_password]
    )
    password2 = serializers.CharField(write_only=True, required=True)
    is_doctor = serializers.BooleanField(required=True)
    date_of_birth = serializers.DateField(required=True)

    def validate(self, attrs):
        if attrs['password'] != attrs['password2']:
            raise serializers.ValidationError(
                {"password": "Password fields didn't match."}
            )

        return attrs

    def create(self, validated_data):
        user = User.objects.create(
            username=validated_data['username'],
            is_doctor=validated_data['is_doctor'],
            date_of_birth=validated_data['date_of_birth'],
        )

        user.set_password(validated_data['password'])
        user.save()
        user.diagnoses.add(*validated_data['diagnoses'])
        return user

    class Meta:
        model = User
        fields = (
            'username',
            'date_of_birth',
            'diagnoses',
            'is_doctor',
            'created_at',
            'password',
            'password2',
        )


class CustomUserListUpdateSerializer(serializers.ModelSerializer):
    diagnoses = DiagnosisSerializer(many=True, read_only=True)
    is_doctor = serializers.BooleanField(required=True)
    date_of_birth = serializers.DateField(required=True)

    class Meta:
        model = User
        fields = ('id', 'is_doctor', 'date_of_birth', 'diagnoses', 'created_at')