from django.contrib.auth.models import AbstractUser
from django.db import models


class Diagnosis(models.Model):
    title = models.CharField(max_length=150)

    def __str__(self):
        return self.title


class CustomUser(AbstractUser):
    is_doctor = models.BooleanField(default=False)
    date_of_birth = models.DateField()
    created_at = models.DateTimeField(auto_now_add=True)
    diagnoses = models.ManyToManyField(
        Diagnosis,
        related_name='users',
    )

    def __str__(self):
        return self.username

