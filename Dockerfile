# syntax=docker/dockerfile:1
FROM python:3.9
ENV PYTHONUNBUFFERED=1

WORKDIR /code
ADD . /code/
RUN pip install -r /code/requirements.txt
